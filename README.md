﻿在线教育，免费教育
====
自我介绍:我是一个三线城市的前端、UI工作者。具备pc站，wap端，微信小程序，混合app开发的能力。想往产品经理的方向发展。但是三线城市没有这个职位，于是乎自己起了一个这个自认为有点意义的项目。
什么调研呀写策划呀 都没干过，git也是刚用就连现在写的markdown都是现学现卖。当然我会把这些学习资料也扔在这里。
#### 好用的数据分析
[友盟](https://www.umeng.com/)  
[七麦](https://www.qimai.cn/)  
[百度指数](http://index.baidu.com)  

#### 大数据分析
[千帆](https://qianfan.analysys.cn/)  
[诸葛io](https://zhugeio.com/)  
[易观方舟](https://ark.analysys.cn/)  
#### [学习用到的材料](https://gitee.com/Endless-reincarnation/free_education/tree/master/%E5%AD%A6%E4%B9%A0%E6%9D%90%E6%96%99)
[在线教育知乎](https://www.zhihu.com/search?type=content&q=%E5%9C%A8%E7%BA%BF%E6%95%99%E8%82%B2)  
[思维导图，流程图，泳道图](https://www.processon.com)
 
**关键词**  
> 网校系统，在线教育，在线教育平台，搭建网校，网上教育平台，网校平台，在线辅导平台，网校搭建，教育云平台，远程教学软件，创建网校，MOOC系统，教育云，网络教育平台，在线授课，在线教育平台有哪些
> ，CRM，报名系统，运营系统，运营平台，教育机构,直播平台，录播课程，在线题库，在线直播，直播课，视频课，题库


&emsp;&emsp;**入住平台**  
&emsp;&emsp;[有道精品课](http://tongdao.youdao.com)  



&emsp;&emsp;**整套解决方案**  
&emsp;&emsp;[保利威-教育整套解决方案](http://www.polyv.net)  
&emsp;&emsp;[edusoho-功能强劲有开源软甲未测试](http://www.edusoho.com/)  
&emsp;&emsp;[拓课云](http://www.talk-cloud.com/)  
&emsp;&emsp;[云朵课堂](http://www.yunduokj.com/bd2pc/zxjy/?jpdy-49)  
&emsp;&emsp;**视频解决方案**  
&emsp;&emsp;[阿里云](https://www.aliyun.com/solution/education?spm=5176.8142029.388261.45.707b6d3eJQf56E)  
&emsp;&emsp;[网易云](http://www.edusoho.com/)  
&emsp;&emsp;[腾讯云](https://cloud.tencent.com/act/industry/video?fromSource=gwzcw.1080936.1080936.1080936)  
&emsp;&emsp;[七牛云](https://www.qiniu.com/products/plsv)  

**在线教育**  
&emsp;&emsp;[学而思](http://www.xueersi.com)  
&emsp;&emsp;[乐学](http://www.lexue.com/)  
&emsp;&emsp;[洋葱数学](https://yangcong345.com)  
&emsp;&emsp;[作业帮](http://zhibo.zuoyebang.com/?fr=ykpz1)  
&emsp;&emsp;[猿辅导](https://www.yuanfudao.com)    
&emsp;&emsp;[学而思](http://www.xueersi.com)  
&emsp;&emsp;[沪江](https://class.hujiang.com)  

国外  
&emsp;&emsp;[class-central多种类](www.class-central.com)  
&emsp;&emsp;[学习创新](http://www.gettingsmart.com/)  
&emsp;&emsp;[教育资源](https://www.teachthought.com)  
**翻转课堂**

**混合式学习**  
&emsp;&emsp;[12种混合式学习，总有一款适合你](https://zhuanlan.zhihu.com/p/33014842)  
&emsp;&emsp;[100多所中学和中学值得一游](http://www.gettingsmart.com/high-schools-worth-visiting/)   
**在线教育弊端**  
&emsp;&emsp;[K12在线教育在国外的发展状况？](https://www.zhihu.com/question/26534050/answer/33125318)  
&emsp;&emsp;[线教育类的app存在什么样的问题](https://www.zhihu.com/question/39209179)  
&emsp;&emsp;[在线教育如何创业](https://zhuanlan.zhihu.com/p/27946202)  
&emsp;&emsp;[百度传课发公告要关闭了我该怎么办？](https://www.zhihu.com/question/265333429/answer/293139522)  

&emsp;&emsp;[K12在线1对1直播辅导平台从0到100搭建经验分享](https://baijiahao.baidu.com/s?id=1609521202531747720&wfr=spider&for=pc)  
&emsp;&emsp;[·智慧教育技术的实践与分享](https://www.zhihu.com/question/265333429/answer/293139522)  
&emsp;&emsp;[分析：作业帮、新东方、学而思等如何提高招生率、续报率](https://36kr.com/p/5149656.html)  
&emsp;&emsp;[新东方在线孙畅：在线教育行业的坚守与创新 | 2018 新风向峰会](https://36kr.com/p/5149562.html)  

1、标准化内容能够解决知识传递的部分问题，大部分APP也只能解决部分环节的沟通、效率、体验等问题，而教育本身有着极强的个性化需求，如何拆解教学目标、如何针对性进行教学内容设计、如何提升学生积极性等）仍然需要专业教师大量碎片化操作来完成；

2、优质教育资源缺乏信息化，三四线城市学生对于课外辅导需求强烈，但当地优秀师资很有限，一线城市优秀师资也只能满足局部需求。系统内对教育资源缺乏统一化的平台管理，造成了学习和分享的困难，系统外已有的信息化教育资源则较为分散，体系渠道不通畅更加剧了信息化教育资源的共享难度。

3、在线教育与线下教育相比较最大的问题在于沉浸性差，缺乏与老师与同学的双向沟通，容易导致学生学习兴趣逐步下降，最终半途而废。


  
